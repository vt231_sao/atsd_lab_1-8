﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <random>
std::vector<int> generateRandomData(int size) {
    std::vector<int> data(size);
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(1, 1000000);

    for (int& value : data) {
        value = distrib(gen);
    }
    return data;
}
void measureSortingTime(int size) {
    auto data = generateRandomData(size);
    auto start = std::chrono::high_resolution_clock::now();
    std::sort(data.begin(), data.end());
    auto end = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> duration = end - start;
    std::cout << "Time to sort " << size << " elements: " << duration.count() << " ms\n";
}
int main() {
    std::vector<int> sizes = { 10, 100, 500, 1000, 2000, 5000, 10000 };
    for (int size : sizes) {
        measureSortingTime(size);
    }
    return 0;
}
