﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>

int main() {

    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    clock_t start = clock();

    srand(time(NULL));

    double numbers[20];

    for (int i = 0; i < 20; i++) {
        numbers[i] = rand() % 100 + (rand() % 100) / 100.0;
    }

    printf("Згенерований масив з 20 чисел:\n");
    for (int i = 0; i < 20; i++) {
        printf("%.2f ", numbers[i]);
    }
    printf("\n");

    double maxValue = numbers[0];
    for (int i = 0; i < 20; i++)
    {
        if (maxValue < numbers[i]) {
            maxValue = numbers[i];
        }
    }

    printf("max = %.2f ", maxValue);

    clock_t end = clock();

    double elapsed_time = (double)(end - start) / CLOCKS_PER_SEC;

    printf("\nЧас виконання програми: %.6f секунд\n", elapsed_time);

    return 0;
}
