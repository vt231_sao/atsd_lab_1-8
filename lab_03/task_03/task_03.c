﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

char* decimalToBinary(int a, int b) {
    char* binary = (char*)malloc(b + 1);
    if (binary == NULL) {
        printf("Помилка виділення пам'яті.\n");
        exit(1);
    }

    int i = b - 1; 

    while (i >= 0) {
        binary[i] = (a & 1) + '0';
        a >>= 1; 
        i--;
    }

    binary[b] = '\0';

    return binary;
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int a, b;
    printf("Введіть ціле число a: ");
    scanf("%d", &a);
    printf("Введіть розрядність b (8 <= b <= 64): ");
    scanf("%d", &b);

    if (b < 8 || b > 64) {
        printf("Неправильна розрядність. Розрядність має бути в діапазоні від 8 до 64.\n");
        return 1;
    }

    char* binary = decimalToBinary(a, b);
    printf("Двійкове представлення числа %d: %s\n", a, binary);

    free(binary);
    return 0;
}
