﻿#include <stdio.h>
#include <math.h>

unsigned long long factorial(int n) {
    unsigned long long result = 1;
    for (int i = 1; i <= n; ++i) {
        result *= i;
    }
    return result;
}

int main() {
    printf(" f(n) = n\n");
    for (int n = 0; n <= 50; ++n) {
        double result1 = n;
        printf("%16.2f\n", result1);
    }

    printf("\n");

    printf(" f(n) = log(n)\n");
    for (int n = 1; n <= 50; ++n) {
        double result2 = log(n);
        printf("%19.2f\n", result2);
    }

    printf("\n");

    printf(" f(n) = n * log(n)\n");
    for (int n = 1; n <= 50; ++n) {
        double result3 = n * log(n);
        printf("%18.2f\n", result3);
    }

    printf("\n");

    printf(" f(n) = n^2\n");
    for (int n = 0; n <= 50; ++n) {
        double result4 = pow(n, 2);
        printf("%17.2f\n", result4);
    }

    printf("\n");

    printf(" f(n) = 2^n\n");
    for (int n = 0; n <= 50; ++n) {
        double result5 = pow(2, n);
        printf("%19.2f\n", result5);
    }

    printf("\n");

    printf(" f(n) = n!\n");
    for (int n = 0; n <= 50; ++n) {
        unsigned long long result6 = factorial(n);
        printf("%17llu\n", result6);
    }

    return 0;
}
