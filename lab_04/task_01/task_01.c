﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

typedef struct Node {
    int data;
    struct Node* prev;
    struct Node* next;
} Node;

Node* createNode(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    if (newNode == NULL) {
        printf("Память не выделена.\n");
        return NULL;
    }
    newNode->data = data;
    newNode->prev = NULL;
    newNode->next = NULL;
    return newNode;
}

void pushFront(Node** head, int data) {
    Node* newNode = createNode(data);
    if (newNode == NULL) return;
    newNode->next = *head;
    if (*head != NULL) {
        (*head)->prev = newNode;
    }
    *head = newNode;
}

void append(Node** head, int data) {
    Node* newNode = createNode(data);
    if (newNode == NULL) return;
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    Node* last = *head;
    while (last->next != NULL) {
        last = last->next;
    }
    last->next = newNode;
    newNode->prev = last;
}

void deleteNode(Node** head, Node* delNode) {
    if (*head == NULL || delNode == NULL) return;
    if (*head == delNode) {
        *head = delNode->next;
    }
    if (delNode->next != NULL) {
        delNode->next->prev = delNode->prev;
    }
    if (delNode->prev != NULL) {
        delNode->prev->next = delNode->next;
    }
    free(delNode);
}
void printList(Node* node) {
    Node* last = NULL;
    printf("Прямий порядок: ");
    while (node != NULL) {
        printf("%d ", node->data);
        last = node;
        node = node->next;
    }
    printf("\nЗворотній порядок: ");
    if (last == NULL) {
        printf("Список пустий\n");
    }
    else {
        while (last != NULL) {
            printf("%d ", last->data);
            last = last->prev;
        }
    }
    printf("\n");
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    Node* head = NULL;
    append(&head, 10);
    pushFront(&head, 20);
    append(&head, 30);
    printList(head);
    deleteNode(&head, head->next);
    printList(head);
    return 0;
}
