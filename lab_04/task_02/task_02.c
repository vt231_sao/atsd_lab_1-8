﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

typedef struct Node {
    int data;
    struct Node* prev;
    struct Node* next;
} Node;

Node* createNode(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    if (newNode == NULL) {
        printf("Память не вдалося виділити.\n");
        return NULL;
    }
    newNode->data = data;
    newNode->prev = NULL;
    newNode->next = NULL;
    return newNode;
}

void append(Node** head, int data) {
    Node* newNode = createNode(data);
    if (newNode == NULL) return;
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    Node* last = *head;
    while (last->next != NULL) {
        last = last->next;
    }
    last->next = newNode;
    newNode->prev = last;
}

void deleteNode(Node** head, int data) {
    if (*head == NULL) return;
    Node* temp = *head;
    while (temp != NULL && temp->data != data) {
        temp = temp->next;
    }
    if (temp == NULL) {
        printf("Елемент не знайдено.\n");
        return;
    }
    if (*head == temp) *head = temp->next;
    if (temp->next != NULL) temp->next->prev = temp->prev;
    if (temp->prev != NULL) temp->prev->next = temp->next;
    free(temp);
}

void printList(Node* node) {
    printf("Список: ");
    while (node != NULL) {
        printf("%d ", node->data);
        node = node->next;
    }
    printf("\n");
}

void destroyList(Node** head) {
    Node* current = *head;
    Node* next = NULL;
    while (current != NULL) {
        next = current->next;
        free(current);
        current = next;
    }
    *head = NULL;
    printf("Список знищено.\n");
}

void dialog() {
    Node* head = NULL;
    int choice, data;
    while (1) {

        printf("1. Додати елемент\n2. Видалити елемент\n3. Вивести список\n4. Знищити список\n5. Вийти\n6. Очистити консоль\nВаш вибір: ");
        scanf("%d", &choice);
        switch (choice) {
        case 1:
            printf("Введіть елемент: ");
            scanf("%d", &data);
            append(&head, data);
            break;
        case 2:
            printf("Введіть елемент для видалення: ");
            scanf("%d", &data);
            deleteNode(&head, data);
            break;
        case 3:
            printList(head);
            break;
        case 4:
            destroyList(&head);
            break;
        case 5:
            destroyList(&head); 
            printf("Вихід з програми...\n");
            return;       
        case 6:
            system("cls");
        default:
            printf("Некоректний вибір. Будь ласка, спробуйте ще раз.\n");
        }

    }
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    dialog();
    return 0;
}
