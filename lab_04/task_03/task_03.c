﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#define MAXOP 100 

typedef struct Stack {
    int top;
    double array[MAXOP];
} Stack;

void initStack(Stack* s) {
    s->top = -1;
}

void push(Stack* s, double element) {
    if (s->top < MAXOP - 1) {
        s->array[++s->top] = element;
    }
    else {
        printf("Переповнення стека\n");
    }
}

double pop(Stack* s) {
    if (s->top != -1) {
        return s->array[s->top--];
    }
    else {
        printf("Пустий стек\n");
        return 0.0;
    }
}

int isNumeric(char* str) {
    if (str == NULL) return 0;
    char* p;
    strtod(str, &p);
    return *p == 0;
}
double performOperation(char op, double a, double b) {
    switch (op) {
    case '+': return a + b;
    case '-': return a - b;
    case '*': return a * b;
    case '/': return a / b;
    case '^': return pow(a, b);
    default: return 0.0;
    }
}

double evaluatePostfix(char* expr) {
    Stack s;
    initStack(&s);
    char* token = strtok(expr, " ");
    while (token != NULL) {
        if (isNumeric(token)) {
            push(&s, atof(token));
        }
        else if (strlen(token) == 1 && strchr("+-*/^", token[0])) {
            double val2 = pop(&s);
            double val1 = pop(&s);
            double result = performOperation(token[0], val1, val2);
            push(&s, result);
        }
        else if (strcmp(token, "sqrt") == 0) {
            double val = pop(&s);
            push(&s, sqrt(val));
        }
        token = strtok(NULL, " ");
    }
    return pop(&s);
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    char expr[] = "7 5 2 - 4 * +";
    char exprCopy[MAXOP];
    strcpy(exprCopy, expr);
    double result = evaluatePostfix(exprCopy);
    printf("Result: %.2f\n", result);
    return 0;
}
