﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <random>
#include <iomanip>
void generate_random_array(std::vector<int>& array) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 9999);

    for (auto& element : array) {
        element = dis(gen);
    }
}

void heapify(std::vector<int>& array, int size, int i) {
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < size && array[left] > array[largest])
        largest = left;

    if (right < size && array[right] > array[largest])
        largest = right;

    if (largest != i) {
        std::swap(array[i], array[largest]);
        heapify(array, size, largest);
    }
}

void heapSort(std::vector<int>& array) {
    int size = array.size();
    for (int i = size / 2 - 1; i >= 0; i--)
        heapify(array, size, i);
    for (int i = size - 1; i >= 0; i--) {
        std::swap(array[0], array[i]);
        heapify(array, i, 0);
    }
}

void shellSort(std::vector<int>& array) {
    int size = array.size();
    for (int gap = size / 2; gap > 0; gap /= 2) {
        for (int i = gap; i < size; i++) {
            int temp = array[i];
            int j;
            for (j = i; j >= gap && array[j - gap] > temp; j -= gap) {
                array[j] = array[j - gap];
            }
            array[j] = temp;
        }
    }
}

void countSort(std::vector<int>& array) {
    int max = *max_element(array.begin(), array.end());
    std::vector<int> count(max + 1, 0);
    std::vector<int> output(array.size());

    for (int number : array) {
        count[number]++;
    }

    for (int i = 1; i <= max; i++) {
        count[i] += count[i - 1];
    }

    for (int i = array.size() - 1; i >= 0; i--) {
        output[count[array[i]] - 1] = array[i];
        count[array[i]]--;
    }

    array = output;
}

int main() {
    std::vector<int> sizes = { 10, 100, 500, 1000, 2000, 5000, 10000 };

    for (int size : sizes) {
        std::vector<int> array(size);
        generate_random_array(array);
        std::vector<int> copy = array;

        auto start = std::chrono::high_resolution_clock::now();
        heapSort(copy);
        auto heap_end = std::chrono::high_resolution_clock::now();

        copy = array;
        auto shell_start = std::chrono::high_resolution_clock::now();
        shellSort(copy);
        auto shell_end = std::chrono::high_resolution_clock::now();

        copy = array;
        auto count_start = std::chrono::high_resolution_clock::now();
        countSort(copy);
        auto count_end = std::chrono::high_resolution_clock::now();

        std::chrono::duration<double> heap_time = heap_end - start;
        std::chrono::duration<double> shell_time = shell_end - shell_start;
        std::chrono::duration<double> count_time = count_end - count_start;

        std::cout << std::fixed << std::setprecision(8); // Встановлює точність до 8 знаків після коми
        std::cout << "Size: " << size << ", HeapSort Time: " << heap_time.count() << " sec, "
            << "ShellSort Time: " << shell_time.count() << " sec, "
            << "CountSort Time: " << count_time.count() << " sec\n";
    }

    return 0;
}