﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <random>

void countingSort(int* array, int size, int range) {
    #define RANGE 10
    int count[RANGE + 1];

    memset(count, 0, sizeof(count));

    for (int i = 0; i < size; i++) {
        count[array[i]]++;
    }

    for (int i = 0, j = 0; i <= range; i++) {
        while (count[i] > 0) {
            array[j] = i;
            j++;
            count[i]--;
        }
    }
}

int main() {
    int arr[] = { 4, 2, 2, 8, 3, 3, 1 };
    int n = sizeof(arr) / sizeof(arr[0]);
    int range = 8; 

    countingSort(arr, n, range);

    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    printf("\n");

    return 0;
}