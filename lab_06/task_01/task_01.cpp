﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <random>
void swap(int* a, int* b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

void siftDown(int* arr, int start, int end) {
    int root = start;

    while ((root * 2 + 1) <= end) {
        int child = root * 2 + 1;
        int swapIdx = root;

        if (arr[swapIdx] < arr[child]) {
            swapIdx = child;
        }
        if (child + 1 <= end && arr[swapIdx] < arr[child + 1]) {
            swapIdx = child + 1;
        }
        if (swapIdx == root) {
            return;
        }
        else {
            swap(&arr[root], &arr[swapIdx]);
            root = swapIdx;
        }
    }
}

void heapSort(int* arr, int len) {
    int count = len;

    for (int i = (count - 2) / 2; i >= 0; i--) {
        siftDown(arr, i, count - 1);
    }

    for (int end = count - 1; end > 0; end--) {
        swap(&arr[end], &arr[0]);
        siftDown(arr, 0, end - 1);
    }
}

int main() {
    int arr[] = { 3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5 };
    int n = sizeof(arr) / sizeof(arr[0]);

    heapSort(arr, n);

    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    printf("\n");

    return 0;
}