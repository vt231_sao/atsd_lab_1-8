﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <random>
#include <limits.h>
#include <stdbool.h>
#include <windows.h>

#define SIZE 19
#define INF INT_MAX

int adjacency_matrix[SIZE][SIZE];
bool visited[SIZE];
int path[SIZE];
int path_index;

void initialize_adjacency_matrix() {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            adjacency_matrix[i][j] = (i == j) ? 0 : INF;
        }
    }


      // Маршрути руху автобусів
    adjacency_matrix[0][1] = 135;
    adjacency_matrix[1][2] = 80;
    adjacency_matrix[2][3] = 100;
    adjacency_matrix[3][4] = 68;  // Київ -> Житомир -> Новоград-Волинський -> Рівно -> Луцьк

    adjacency_matrix[1][5] = 38;
    adjacency_matrix[5][6] = 73;
    adjacency_matrix[6][7] = 110;
    adjacency_matrix[7][8] = 104; // Київ -> Житомир -> Бердичів -> Вінниця -> Хмельницький -> Тернопіль

    adjacency_matrix[1][9] = 115; // Київ -> Житомир -> Шепетівка

    adjacency_matrix[0][10] = 78;
    adjacency_matrix[10][11] = 115; // Київ -> Біла Церква -> Умань

    adjacency_matrix[10][12] = 146;
    adjacency_matrix[12][13] = 105; // Київ -> Біла Церква -> Черкаси -> Кременчук

    adjacency_matrix[10][14] = 181;
    adjacency_matrix[14][15] = 130; // Київ -> Біла Церква -> Полтава -> Харків

    adjacency_matrix[0][16] = 128;
    adjacency_matrix[16][17] = 175; // Київ -> Прилуки -> Суми

    adjacency_matrix[16][18] = 109; // Київ -> Прилуки -> Миргород
}

void print_path() {
    int total_distance = 0;
    printf("Маршрут: ");
    for (int i = 0; i < path_index; i++) {
        printf("%d -> ", path[i]);
        if (i < path_index - 1)
            total_distance += adjacency_matrix[path[i]][path[i + 1]];
    }
    printf("%d\n", path[path_index]);
    printf("Загальна відстань: %d км\n\n", total_distance);
}

void dfs(int start, int depth) {
    visited[start] = true;
    path[path_index++] = start;

    if (depth > 0) print_path();

    for (int i = 0; i < SIZE; i++) {
        if (adjacency_matrix[start][i] != INF && !visited[i]) {
            dfs(i, depth + 1);
        }
    }

    visited[start] = false;
    path_index--;
}

void bfs(int start) {
    int queue[SIZE], front = 0, rear = 0, current;
    queue[rear++] = start;
    visited[start] = true;

    printf("BFS відвідування: ");
    while (front < rear) {
        current = queue[front++];
        printf("%d ", current);

        for (int i = 0; i < SIZE; i++) {
            if (adjacency_matrix[current][i] != INF && !visited[i]) {
                visited[i] = true;
                queue[rear++] = i;
            }
        }
    }
    printf("\n");
}

int main() {

    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    initialize_adjacency_matrix();
    int start_vertex = 0;
    printf("Введіть номер початкової вершини для DFS і BFS (0 - Київ): ");
    scanf("%d", &start_vertex);

    printf("\nDFS від вершини %d:\n", start_vertex);
    path_index = 0;
    dfs(start_vertex, 0);

    printf("\nBFS від вершини %d:\n", start_vertex);
    for (int i = 0; i < SIZE; i++) visited[i] = false; // Скидаємо стан відвідуваності для BFS
    bfs(start_vertex);

    return 0;
}