﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <cstdint>
#include <windows.h>

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    size_t size_tm = sizeof(struct tm);
    printf("Розмір структури tm з time.h: %zu байт\n", size_tm);

    return 0;
}