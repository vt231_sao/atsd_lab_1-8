﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <cstdint>
#include <windows.h>

union CompactFloat {
    float f;
    struct {
        unsigned int mantissa : 23; // Мантиса, 23 біти
        unsigned int exponent : 8;  // Ступінь, 8 біт
        unsigned int sign : 1;      // Знак, 1 біт
    } parts;
};


void displayBitwise(union CompactFloat cf) {
    printf("Значення побітово: %x\n", *((unsigned int*)&cf));
}
void displayBytewise(union CompactFloat cf) {
    printf("Значення побайтово: %02x %02x %02x %02x\n",
        ((unsigned char*)&cf)[3], ((unsigned char*)&cf)[2], ((unsigned char*)&cf)[1], ((unsigned char*)&cf)[0]);
}

void displayParts(union CompactFloat cf) {
    printf("Знак: %d\n", cf.parts.sign);
    printf("Мантиса: %d\n", cf.parts.mantissa);
    printf("Ступінь: %d\n", cf.parts.exponent);
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    union CompactFloat cf;
    float input;

    printf("Введіть дійсне число типу float: ");
    scanf("%f", &input);

    cf.f = input;

    displayBitwise(cf);
    displayBytewise(cf);
    displayParts(cf);

    printf("Обсяг пам'яті, яку займає змінна користувацького типу: %zu байт\n", sizeof(union CompactFloat));

    return 0;
}