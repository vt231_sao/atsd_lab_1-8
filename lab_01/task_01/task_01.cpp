﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <cstdint>
#include <windows.h>
// Структура
struct CompactDateTime {
    uint16_t year;    // Рік
    uint8_t month;    // Місяць (1-12)
    uint8_t day;      // День (1-31)
    uint8_t hour;     // Година (0-23)
    uint8_t minute;   // Хвилина (0-59)
    uint8_t second;   // Секунда (0-59)
};

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    struct CompactDateTime currentTime = { 2024, 2, 18, 21, 28, 45 };
    size_t size = sizeof(struct CompactDateTime);
    printf("Розмір структури CompactDateTime: %zu байт\n", size);

    printf("Поточний час і дата: %d-%02d-%02d %02d:%02d:%02d\n",
        currentTime.year, currentTime.month, currentTime.day,
        currentTime.hour, currentTime.minute, currentTime.second);

    return 0;
}
