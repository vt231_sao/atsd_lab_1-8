﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <cstdint>
#include <windows.h>


int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    short input;
    unsigned short value;
    unsigned short sign;

    printf("Введіть ціле число: ");
    scanf("%hd", &input);

    sign = (input < 0) ? 1 : 0;
    value = (sign == 1) ? -input : input; 
   
    printf("Значення: %hu\n", value);
    printf("Знак: %s\n", sign ? "від'ємне" : "позитивне");

    return 0;
}