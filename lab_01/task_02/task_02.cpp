﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <cstdint>
#include <windows.h>

struct SignedShort {
    unsigned short value : 15; 
    unsigned short sign : 1;  
};

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    struct SignedShort ss;
    short input;

    printf("Введіть ціле число: ");
    scanf("%hd", &input);

    if (input < 0) {
        ss.sign = 1; 
        ss.value = -input;
    }
    else {
        ss.sign = 0; 
        ss.value = input;
    }

    printf("Значення: %hu\n", ss.value);
    printf("Знак: %s\n", ss.sign ? "від'ємне" : "позитивне");

    return 0;
}