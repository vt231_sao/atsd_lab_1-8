﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <cstdint>
#include <windows.h>


int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    signed char a = 5;
    signed char b = 127;
    signed char c = -120;
    signed char result;

    // a) 5 + 127
    result = a + b;
    printf("a) 5 + 127 = %d\n", result);

    // б) 2 - 3
    result = 2 - 3;
    printf("б) 2 - 3 = %08x\n", result);

    // в) -120 - 34
    result = c - 34;
    printf("в) -120 - 34 = %08x\n", result);

    // г) (unsigned char) (-5)
    result = (signed char)((unsigned char)-5);
    printf("г) (unsigned char)(-5) = %08x\n", result);

    // д) 56 & 38
    result = 56 & 38;
    printf("д) 56 & 38 = %08x\n", result);

    // е) 56 | 38
    result = 56 | 38;
    printf("е) 56 | 38 = %08x\n", result);

    return 0;
}