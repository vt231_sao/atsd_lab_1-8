﻿#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <windows.h>

unsigned int nextValues(unsigned int a, unsigned int seed, unsigned int c, unsigned int m) {
    return (a * seed + c) % m;
}
// Розрахунок середньоквадратичного відхилення випадкових величин
float standard_deviation(float variance) {
    return sqrt(variance);
}
//Матиматичне сподівання
float mean(unsigned int num_values) {
    float mean_value = 0;
    float probability = 1.0f / num_values; 
    for (unsigned int i = 0; i < num_values; ++i) {
        mean_value += i * probability;
    }
    return mean_value;
}


// Розрахунок дисперсії випадкових величин
float variance(int frequencies[], unsigned int num_values, float mean_value, unsigned int total_observations) {
    float variance_value = 0;
    for (unsigned int i = 0; i < num_values; ++i) {
        variance_value += pow(i - mean_value, 2) * ((float)frequencies[i] / total_observations);
    }
    return variance_value;
}
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    unsigned int a = 1140671485, c = 12820163, m = 16777216; 
    const unsigned int K = 20000;
    const unsigned int n = 250; 
    unsigned int X0 = 0;
    unsigned int seed = X0;
    // Розрахунок частоти інтервалів
    unsigned int values[K]; 
    int frequency[n] = { 0 }; 

    // Генерація послідовності та підрахунок частоти інтервалів
    for (unsigned int i = 0; i < K; i++) {
        unsigned int X1 = nextValues(a, seed, c, m);
        unsigned int interval = X1 % n; 
        values[i] = X1;
        frequency[interval]++; 
        seed = X1;
    }

    printf("Частота інтервалів появи випадкових величин:\n");
    for (unsigned int i = 0; i < n; ++i) {
        printf("Інтервал %u: %d\n", i, frequency[i]);
    }
    // Розрахунок статистичної імовірності
    unsigned int total_observations = K;

    // Розрахунок математичного сподівання
    float mean_value = mean(n);
    printf("Математичне сподівання: %.2f\n", mean_value);

    // Розрахунок дисперсії
    float variance_value = variance(frequency, n, mean_value, total_observations);    
    printf("Дисперсія: %.2f\n", variance_value); 
    // Розрахунок середньоквадратичного відхилення
    float st_dev = standard_deviation(variance_value);
    printf("Середньоквадратичне відхилення: %.2f\n", st_dev);

    
    return 0;
}
